const header = document.querySelector(".calendar h3");
const dates = document.querySelector(".dates");
const navs = document.querySelectorAll("#prev, #next");

const months = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];

let date = new Date();
let month = date.getMonth();
let year = date.getFullYear();

function renderCalendar() {
  const start = new Date(year, month, 1).getDay();
  const endDate = new Date(year, month + 1, 0).getDate();
  const end = new Date(year, month, endDate).getDay();
  const endDatePrev = new Date(year, month, 0).getDate();

  let datesHtml = "";

  for (let i = start; i > 0; i--) {
    datesHtml += `<li class="inactive">${endDatePrev - i + 1}</li>`;
  }

  for (let i = 1; i <= endDate; i++) {
    let className =
      i === date.getDate() &&
      month === new Date().getMonth() &&
      year === new Date().getFullYear()
        ? ' class="today"'
        : "";
    datesHtml += `<li${className}>${i}</li>`;
  }

  for (let i = end; i < 6; i++) {
    datesHtml += `<li class="inactive">${i - end + 1}</li>`;
  }

  dates.innerHTML = datesHtml;
  header.textContent = `${months[month]} ${year}`;
}

navs.forEach((nav) => {
  nav.addEventListener("click", (e) => {
    const btnId = e.target.id;

    if (btnId === "prev" && month === 0) {
      year--;
      month = 11;
    } else if (btnId === "next" && month === 11) {
      year++;
      month = 0;
    } else {
      month = btnId === "next" ? month + 1 : month - 1;
    }

    date = new Date(year, month, new Date().getDate());
    year = date.getFullYear();
    month = date.getMonth();

    renderCalendar();
  });
});

renderCalendar();

// Код для первого слайдера
let slideIndex1 = 0;

function showSlide1(n) {
    const slides = document.querySelectorAll('#slider1-container .slider-slide');
    if (n >= slides.length) {
        slideIndex1 = 0;
    } else if (n < 0) {
        slideIndex1 = slides.length - 1;
    } else {
        slideIndex1 = n;
    }
    slides.forEach((slide, index) => {
        slide.style.display = (index === slideIndex1) ? 'flex' : 'none';
        setTimeout(showSlides, 2000); // Change slide every 2 seconds
    });
}

function nextSlide1() {
    showSlide1(slideIndex1 + 1);
}

function prevSlide1() {
    showSlide1(slideIndex1 - 1);
}


// Код для второго слайдера
let slideIndex2 = 0;

function showSlide2(n) {
    const slides = document.querySelectorAll('#slider2-container .slider-slide');
    if (n >= slides.length) {
        slideIndex2 = 0;
    } else if (n < 0) {
        slideIndex2 = slides.length - 1;
    } else {
        slideIndex2 = n;
    }
    slides.forEach((slide, index) => {
        slide.style.display = (index === slideIndex2) ? 'flex' : 'none';
    });
}

function nexteSlide2() {
    showSlide2(slideIndex2 + 1);
}

function preveSlide2() {
    showSlide2(slideIndex2 - 1);
}

// Инициализация слайдеров
showSlide1(slideIndex1);
showSlide2(slideIndex2);

// Оригинальный слайдер
let slideIndex = 1;
showSlides(slideIndex);

function nextSlide() {
    showSlides(slideIndex += 1);
}

function previousSlide() {
    showSlides(slideIndex -= 1);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let slides = document.getElementsByClassName("item");
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    for (let slide of slides) {
        slide.style.display = "none";
    }
    slides[slideIndex - 1].style.display = "flex";
}

document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('openPopupButton').addEventListener('click', function() {
      document.getElementById('popup').style.display = 'flex';
  });

  document.getElementById('closePopupButton').addEventListener('click', function() {
      document.getElementById('popup').style.display = 'none';
  });
});
