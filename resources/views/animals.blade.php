<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Баба Фрося</title>
        <link rel="stylesheet"  href="/style.css">
    </head>
    <body>
    <header>
      <img class="logo" src="img/logo.png">
      <h1>Баба Фрося</h1>
      <a id="openPopupButton">Купить билет</a>
      <a href="news">Афиша</a>
      <a href="animals">Животные</a>
      <a href="map">Как добраться</a>
      <a href="aboutus">О нас</a>
      <a href="help">Поддержать зоопарк</a>
    </header>
    <a href="home" class="a">Главная</a>
    <main>
    <section class="section" id="alpaca">
            <img src="img/an1.png" alt="Баба Фрося">
            <div class="content">
                <h2>Капибара</h2>
                <p>Самый большой грызун на планете.
                     Может весить до 60ти кг.
                      Живут возле водоёмов. Очень хорошо плавают.
                       Легко приручаются и одомашниваются. 
                       Капибары проявляют дружелюбие и приветливость и по отношению к другим видам.
                        Известно множество случаев, когда водосвинки принимают в свои группы животных, 
                        которые остались без родителей или были брошены хозяевами. 
                        Они позволяют кататься на своих спинах птицам, кроликам и даже обезьянами в дикой природе,
                         а в неволе дружат с кошками и собаками.</p>
            </div>
        </section>
        <section class="section" id="lion">
            <img src="img/an2.png" alt="Лев">
            <div class="content">
                <h2>Двугорбый верблюд</h2>
                <p>Живут в зоопарке 11 лет. Приехали 
                    малышами. Потомство приносят один раз в два года. Верблюды — спокойные и в ос
                    новном неагрессивные млеко
                    питающие, активны преимущественно днем. Держатся стаями, и в часы отдыха «часовой» — вожак охраняет покой сородичей. При внешней неповоротливости и сомнительной грации верблюды довольно быстро бегают — развить скорость около 60 километров в час для них не проблема.
                    Человек одомашнил верблюда более тысячи л
                    ет назад. Помимо высокой выносливости и способ
                    ности поднимать тяжеловесные грузы, верблюд дает людям жирное и питат
                    ельное молоко, а его навоз используется для разведения костров в пустыне.</p>
            </div>
        </section>
    </main>
    <div class="subscription-container">
        <h2>Подпишитесь на рассылку</h2>
        <form class="subscription-form">
            <input type="email" placeholder="E-mail">
            <button type="submit">Подписаться</button>
        </form>
        <div class="subscription-terms">
            Нажимая на кнопку "Подписаться", вы соглашаетесь с офертой и политикой конфиденциальности
        </div>
    </div>
    <footer class="footer">
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Купить билет</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Отзывы</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Афиша</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Правила</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Поддержать зоопарк</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Вакансии</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">О нас</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Политика конфиденциальности</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Животные</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Ресторан</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Бассейн</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Галерея</a></li>
            </ul>
        </div>
        
        <div class="footer-column contact-info">
            <p>+7 (8512) 29-73-00</p>
            <p>Ежедневно с 11:00 до 21:00</p>
            <p>Зоопарк, открытые бассейны, концерты, фестивали, кафе, беседки.</p>
            <div class="social-icons">
            <a href="https://vk.com/zoopark.babafrosya"><img src="img/vk.png" alt="VK"></a>
                <a href="https://vk.link/zoopark.babafrosya"><img src="img/tg.png" alt="Telegram"></a>
            </div>
        </div>
    </footer>

    <div class="popup" id="popup" style="display: none;">
        <div class="popup-content">
            <h1>Корзина</h1>
            <p class="visit-date">Дата посещения: 05.06.23</p>
            <div class="ticket">
                <span>1 Детский</span>
                <div class="ticket-controls">
                    <button>-</button>
                    <span>1</span>
                    <button>+</button>
                </div>
                <span>250 ₽</span>
            </div>
            <div class="ticket">
                <span>1 Взрослый</span>
                <div class="ticket-controls">
                    <button>-</button>
                    <span>1</span>
                    <button>+</button>
                </div>
                <span>500 ₽</span>
            </div>
            <div class="promo-code">
                <input type="text" placeholder="Промокод">
                <button>Применить</button>
            </div>
            <div class="total">
                <span>Итого</span>
                <span>750 ₽</span>
            </div>
            <input type="email" placeholder="Укажите email для получения билетов">
            <button class="pay-button">Перейти к оплате</button>
            <div class="agreement">
                <input type="checkbox" id="agreement">
                <label for="agreement">Я даю согласие на Обработку персональных данных и соглашаюсь с политикой конфиденциальности</label>
            </div>
            <button id="closePopupButton">Закрыть</button>
        </div>
    </div>
    <script src="script.js" defer></script>
    </body>
</html>
