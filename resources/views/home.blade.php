<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Баба Фрося</title>
        <link rel="stylesheet"  href="/style.css">
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    </head>
    <body>
    <header>
      <img class="logo" src="img/logo.png">
      <h1>Баба Фрося</h1>
      <a id="openPopupButton">Купить билет</a>
      <a href="news">Афиша</a>
      <a href="animals">Животные</a>
      <a href="map">Как добраться</a>
      <a href="aboutus">О нас</a>
      <a href="help">Поддержать зоопарк</a>
    </header>
    <div class="slider">
        <div class="item">
            <a class="previous" onclick="previousSlide()">&#10094;</a>
            <div class="item_col">
               <h2>Ждём в гости</h2>
            
               <a  class="btn">Купить билет</a>
            </div>           
            <img src="img/kopy.png" alt="">
            <a class="next" onclick="nextSlide()">&#10095;</a>
        </div>

        <div class="item">
            <a class="previous" onclick="previousSlide()">&#10094;</a>
            <div class="item_col">
               <h2>Помоги животным</h2>
               
               <a href="help" class="btn">Возьми опеку</a>
            </div>           
            <img src="img/kopy2.png" alt="">
            <a class="next" onclick="nextSlide()">&#10095;</a>
        </div>

        <div class="item">
            <a class="previous" onclick="previousSlide()">&#10094;</a>
            <div class="item_col">
               <h2>Лето в самом разгаре</h2>
              
               <a  href="aboutus" class="btn">Поплавать</a>
            </div>           
            <img src="img/kopy3.png" alt="">
            <a class="next" onclick="nextSlide()">&#10095;</a>
        </div>
        
        <div class="arrow_mobile">
            <a class="previous_mobile" onclick="previousSlide()">&#10094;</a>
            <a class="next_mobile" onclick="nextSlide()">&#10095;</a>
        </div>
        
    </div>
    <div class="vizit">Спланировать визит</div>
    <div class="back">
    <div class="group-59">
  <img class="rtbs-97-oj-2-u-0-1" src="img/ama.png" />
  <img class="p-hn-li-o-4-x-6-p-4-1" src="img/tur.png" />
  <div class="p-hn-li-o-4-x-6-p-4-2"></div>
  <div class="tur">Черепаха</div>
  <div class="ama">Амазон</div>
  <div class="_15-00">
    Время
    <br />
    15:00
  </div>
  <div class="_11-00">
    Время
    <br />
    11:00
  </div>
  <img class="vector-1" src="img/Vector 1.png" />
  <div class="turtel">
    Возраст черепах - 200 млн. лет. Современные черепахи похожи на своих
    предков. Однако отличие все-таки есть – отсутствие зубов и умение прятать
    голову.
    <br />
    Гигантские черепахи населяли нашу планету 70 млн. лет назад.Среди
    современных черепах тоже есть гиганты. Это кожистые черепахи, места обитания
    которых – моря и океаны. Вес кожистых черепах – от 300 кг до полтонны.
  </div>
  <div class="amazon">
    Амазон, или амазонский попугай - популярный вид крупных попугаев. Яркие,
    болтливые и жизнерадостные птицы являются отличными питомцами как для детей,
    так и для взрослых, и привносят в атмосферу дома позитив и ощущение
    праздника.
    <br />
    Основной цвет птиц – зеленый, но у каждого вида есть своя отличительная
    черта – цветные пятна на разных частях тела - голове, хвосте, крыльях.
  </div>
</div>

    <div class="calendar">
    

        <h3></h3>
        <nav>
          <button id="prev"></button>
          <button id="next"></button>
        </nav>
     
      <section>
        <ul class="days">
          <li>Пн</li>
          <li>Вт</li>
          <li>Ср</li>
          <li>Чт</li>
          <li>Пт</li>
          <li>Сб</li>
          <li>Вс</li>
        </ul>
        <ul class="dates"></ul>
      </section>
    </div>
    </div>
    <div class="new">
        <div class="news">Новости</div>
        <div class="zoo">О зоопарке</div>
        <img class="new1" src="img/kopy4.png" />
        <div class="_05">05 июня</div>
        <div class="_03">03 июня</div>
        <div class="_01">01 июня</div>
        <div class="sum">Лето началось. Наши бассейны ждут вас.</div>
        <div class="mibib">У медведя Потапа скоро день рождения.</div>
        <div class="ptic">Прибавление на нашем птичьем дворе.</div>
        <img class="new2" src="img/kopy5.png" />
        <div class="pod">Подробнее</div>
        <img class="new4" src="img/kopy7.png" />
        <img class="new3" src="img/kopy6.png" />
    </div>
    <div class="info">
        На площади в пять гектаров собрана коллекция более пятидесяти видов животных, которая пополняется каждый год. Здесь можно встретить верблюдов, лам, кабанов, сайгаков, рысей, енотов, нутрий, бурого медведя, хохлатого дикобраза, тибетских и индийских буйволов, мексиканских носух, а также домашних животных — осликов, коз, овец, лошадей, павлинов, фазанов, перепелов и страусов. Часть зоопарка представлена в формате контактного зоопарка, где посетители могут потрогать и покормить животных.
    </div>
<div class="sliderw" id="sliderw">
    <div class="slidesw">
        <div class="slidew">
            <div class="review">
                <h3>Наталья</h3>
                <div class="stars">★★★★★</div>
                <p>Просто отличное место для отдыха душой и телом, чисто, уютно, животные ухоженные, вальеры просторные, появились новые территории.</p>
            </div>
        </div>
        <div class="slidew">
            <div class="review">
                <h3>Ольга</h3>
                <div class="stars">★★★★★</div>
                <p>Спасибо кто основал и содержит этот замечательный зоопарк, дети и взрослые в восторге, особенно увидев недавно капибар.</p>
            </div>
            
        </div>
        <div class="slidew">
            <div class="review">
                <h3>Евгений</h3>
                <div class="stars">★★★★★</div>
                <p>Хороший зоопарк и единственный в своем роде в Астраханской области. Детям тут понравится, да и взрослым тоже будет интересно.</p>
            </div>
        </div>
        <div class="slidew">
            <div class="review">
                <h3>Алина</h3>
                <div class="stars">★★★★</div>
                <p>На первое сентября сделали ребёнку сюрприз. Решили поехать в зоопарк Баба Фрося. </p>
            </div>
        </div>
        <div class="slidew">
            <div class="review">
                <h3>Ирина</h3>
                <div class="stars">★★★★★</div>
                <p>Правда хороший зоопарк, животные выглядят сытыми, чистыми и здоровыми. Животных много: как обычных так и экзотических.</p>
            </div>
        </div>
    </div>
</div>


<div class="buttons">
    <button onclick="preve()">Предыдущий</button>
    <button onclick="nexte()">Следующий</button>
</div>
<div class="subscription-container">
        <h2>Подпишитесь на рассылку</h2>
        <form class="subscription-form">
            <input type="email" placeholder="E-mail">
            <button type="submit">Подписаться</button>
        </form>
        <div class="subscription-terms">
            Нажимая на кнопку "Подписаться", вы соглашаетесь с офертой и политикой конфиденциальности
        </div>
    </div>
    <footer class="footer">
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Купить билет</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Отзывы</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Афиша</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Правила</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Поддержать зоопарк</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Вакансии</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">О нас</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Политика конфиденциальности</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Животные</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Ресторан</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Бассейн</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Галерея</a></li>
            </ul>
        </div>
        <div class="footer-column contact-info">
            <p>+7 (8512) 29-73-00</p>
            <p>Ежедневно с 11:00 до 21:00</p>
            <p>Зоопарк, открытые бассейны, концерты, фестивали, кафе, беседки.</p>
            <div class="social-icons">
                <a href="https://vk.com/zoopark.babafrosya"><img src="img/vk.png" alt="VK"></a>
                <a href="https://vk.link/zoopark.babafrosya"><img src="img/tg.png" alt="Telegram"></a>
            </div>
        </div>
    </footer>
    <div class="popup" id="popup" style="display: none;">
        <div class="popup-content">
            <h1>Корзина</h1>
            <p class="visit-date">Дата посещения: 05.06.23</p>
            <div class="ticket">
                <span>1 Детский</span>
                <div class="ticket-controls">
                    <button>-</button>
                    <span>1</span>
                    <button>+</button>
                </div>
                <span>250 ₽</span>
            </div>
            <div class="ticket">
                <span>1 Взрослый</span>
                <div class="ticket-controls">
                    <button>-</button>
                    <span>1</span>
                    <button>+</button>
                </div>
                <span>500 ₽</span>
            </div>
            <div class="promo-code">
                <input type="text" placeholder="Промокод">
                <button>Применить</button>
            </div>
            <div class="total">
                <span>Итого</span>
                <span>750 ₽</span>
            </div>
            <input type="email" placeholder="Укажите email для получения билетов">
            <button class="pay-button">Перейти к оплате</button>
            <div class="agreement">
                <input type="checkbox" id="agreement">
                <label for="agreement">Я даю согласие на Обработку персональных данных и соглашаюсь с политикой конфиденциальности</label>
            </div>
            <button id="closePopupButton">Закрыть</button>
        </div>
    </div>



    <script src="script.js" defer></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    </body>
</html>


