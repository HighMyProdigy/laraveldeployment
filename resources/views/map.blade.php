<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Баба Фрося</title>
        <link rel="stylesheet"  href="/style.css">
        <script src="https://api-maps.yandex.ru/v3/?apikey=227598ed-33be-485b-b6c6-5b8812faa2ae
&lang=ru_RU"></script>
        <script>
            initMap();

async function initMap() {
    // Промис `ymaps3.ready` будет зарезолвлен, когда загрузятся все компоненты основного модуля API
    await ymaps3.ready;

    const {YMap, YMapDefaultSchemeLayer} = ymaps3;

    // Иницилиазируем карту
    const map = new YMap(
        // Передаём ссылку на HTMLElement контейнера
        document.getElementById('map'),

        // Передаём параметры инициализации карты
        {
            location: {
                // Координаты центра карты
                center: [46.08630127297778,47.95953817115651],

                // Уровень масштабирования
                zoom: 10
            }
        }
    );

    // Добавляем слой для отображения схематической карты
    map.addChild(new YMapDefaultSchemeLayer());
}

        </script>
    </head>
    <body>
    <header>
      <img class="logo" src="img/logo.png">
      <h1>Баба Фрося</h1>
      <a id="openPopupButton">Купить билет</a>
      <a href="news">Афиша</a>
      <a href="animals">Животные</a>
      <a href="map">Как добраться</a>
      <a href="aboutus">О нас</a>
      <a href="help">Поддержать зоопарк</a>
    </header>
    <a href="home" class="a">Главная</a>
    <div  id="map" class="map"></div>

    <div class="subscription-container">
        <h2>Подпишитесь на рассылку</h2>
        <form class="subscription-form">
            <input type="email" placeholder="E-mail">
            <button type="submit">Подписаться</button>
        </form>
        <div class="subscription-terms">
            Нажимая на кнопку "Подписаться", вы соглашаетесь с офертой и политикой конфиденциальности
        </div>
    </div>
    <footer class="footer">
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Купить билет</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Отзывы</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Афиша</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Правила</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Поддержать зоопарк</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Вакансии</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">О нас</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Политика конфиденциальности</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Животные</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Ресторан</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Бассейн</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Галерея</a></li>
            </ul>
        </div>
        
        <div class="footer-column contact-info">
            <p>+7 (8512) 29-73-00</p>
            <p>Ежедневно с 11:00 до 21:00</p>
            <p>Зоопарк, открытые бассейны, концерты, фестивали, кафе, беседки.</p>
            <div class="social-icons">
                <a href="https://vk.com/zoopark.babafrosya"><img src="img/vk.png" alt="VK"></a>
                <a href="https://vk.link/zoopark.babafrosya"><img src="img/tg.png" alt="Telegram"></a>
            </div>
        </div>
    </footer>

    <div class="popup" id="popup" style="display: none;">
        <div class="popup-content">
            <h1>Корзина</h1>
            <p class="visit-date">Дата посещения: 05.06.23</p>
            <div class="ticket">
                <span>1 Детский</span>
                <div class="ticket-controls">
                    <button>-</button>
                    <span>1</span>
                    <button>+</button>
                </div>
                <span>250 ₽</span>
            </div>
            <div class="ticket">
                <span>1 Взрослый</span>
                <div class="ticket-controls">
                    <button>-</button>
                    <span>1</span>
                    <button>+</button>
                </div>
                <span>500 ₽</span>
            </div>
            <div class="promo-code">
                <input type="text" placeholder="Промокод">
                <button>Применить</button>
            </div>
            <div class="total">
                <span>Итого</span>
                <span>750 ₽</span>
            </div>
            <input type="email" placeholder="Укажите email для получения билетов">
            <button class="pay-button">Перейти к оплате</button>
            <div class="agreement">
                <input type="checkbox" id="agreement">
                <label for="agreement">Я даю согласие на Обработку персональных данных и соглашаюсь с политикой конфиденциальности</label>
            </div>
            <button id="closePopupButton">Закрыть</button>
        </div>
    </div>
    <script src="script.js" defer></script>
    </body>
</html>
