<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Баба Фрося</title>
        <link rel="stylesheet"  href="/style.css">
    </head>
    <body>
    <header>
      <img class="logo" src="img/logo.png">
      <h1>Баба Фрося</h1>
      <a id="openPopupButton">Купить билет</a>
      <a href="news">Афиша</a>
      <a href="animals">Животные</a>
      <a href="map">Как добраться</a>
      <a href="aboutus">О нас</a>
      <a href="help">Поддержать зоопарк</a>
    </header>
    <a href="home" class="a">Главная</a>
    <main>
    <section class="section" id="alpaca">
            <img src="img/n1.png" alt="Баба Фрося">
            <div class="content">
                <h2>Лето началось. Наши бассейны ждут вас.</h2>
                <p>Устали от изнуряющей астраханской жары?
                     Надоело что город это большая сковородка?
                      Хочется скорее отдохнуть телом и душой на природе,
                       не лишая себя возможности пользоваться благами прогресса?
                        Тогда скорее приезжайте семьей и друзьями в Бабу Фросю.
                         4 открытых бассейна, всегда свободные лежаки,
                     бар и навесы. Это всё уже ждет именно тебя. Не упусти своё лето!</p>
            </div>
        </section>
        <section class="section" id="lion">
            <img src="img/n2.png" alt="Лев">
            <div class="content">
                <h2>У медведя Потапа скоро день рождения.</h2>
                <p>У нашего медведя Потапа в следующем году юбилей. 15 лет. Давайте все вместе сделаем ему подарок.</p>
            </div>
        </section>
    </main>
    <div class="subscription-container">
        <h2>Подпишитесь на рассылку</h2>
        <form class="subscription-form">
            <input type="email" placeholder="E-mail">
            <button type="submit">Подписаться</button>
        </form>
        <div class="subscription-terms">
            Нажимая на кнопку "Подписаться", вы соглашаетесь с офертой и политикой конфиденциальности
        </div>
    </div>
   
    <footer class="footer">
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Купить билет</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Отзывы</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Афиша</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Правила</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Поддержать зоопарк</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Вакансии</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">О нас</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Политика конфиденциальности</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <ul>
                <li><a href="#" style="color: white; text-decoration: none;">Животные</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Ресторан</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Бассейн</a></li>
                <li><a href="#" style="color: white; text-decoration: none;">Галерея</a></li>
            </ul>
        </div>
        
        <div class="footer-column contact-info">
            <p>+7 (8512) 29-73-00</p>
            <p>Ежедневно с 11:00 до 21:00</p>
            <p>Зоопарк, открытые бассейны, концерты, фестивали, кафе, беседки.</p>
            <div class="social-icons">
            <a href="https://vk.com/zoopark.babafrosya"><img src="img/vk.png" alt="VK"></a>
                <a href="https://vk.link/zoopark.babafrosya"><img src="img/tg.png" alt="Telegram"></a>
            </div>
        </div>
    </footer>

   
    <script src="script.js" defer></script>
    <script>
        // Загрузка содержимого всплывающего окна
        fetch('popup.html')
            .then(response => response.text())
            .then(data => {
                document.getElementById('popup-container').innerHTML = data;
            });
    </script>
    </body>
</html>
