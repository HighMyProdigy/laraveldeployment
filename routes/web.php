<?php

use Illuminate\Support\Facades\Route;

Route::get('/home', function () {
    return view('home');
});
Route::get('/map', function () {
    return view('map');
});
Route::get('/animals', function () {
    return view('animals');
});
Route::get('/aboutus', function () {
    return view('aboutus');
});
Route::get('/help', function () {
    return view('help');
});
Route::get('/news', function () {
    return view('news');
});
